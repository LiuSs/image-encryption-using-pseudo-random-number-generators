﻿namespace Image_Encryption_PRNG_
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox_originalImage = new System.Windows.Forms.PictureBox();
            this.Btn_Input = new System.Windows.Forms.Button();
            this.pictureBox_encryptImage = new System.Windows.Forms.PictureBox();
            this.Btn_encryption = new System.Windows.Forms.Button();
            this.pictureBox_scrambleImage = new System.Windows.Forms.PictureBox();
            this.Btn_scramble = new System.Windows.Forms.Button();
            this.pictureBox_decryptImage = new System.Windows.Forms.PictureBox();
            this.Btm_recover = new System.Windows.Forms.Button();
            this.Btn_calculate = new System.Windows.Forms.Button();
            this.Lab_NPCR = new System.Windows.Forms.Label();
            this.Lab_red = new System.Windows.Forms.Label();
            this.Lab_green = new System.Windows.Forms.Label();
            this.Lab_blue = new System.Windows.Forms.Label();
            this.Btn_ASGPcreate = new System.Windows.Forms.Button();
            this.Txt_primeNumber = new System.Windows.Forms.TextBox();
            this.Btn_count = new System.Windows.Forms.Button();
            this.Btn_replace = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_originalImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_encryptImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_scrambleImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_decryptImage)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox_originalImage
            // 
            this.pictureBox_originalImage.BackColor = System.Drawing.Color.Gold;
            this.pictureBox_originalImage.Location = new System.Drawing.Point(12, 3);
            this.pictureBox_originalImage.Name = "pictureBox_originalImage";
            this.pictureBox_originalImage.Size = new System.Drawing.Size(413, 315);
            this.pictureBox_originalImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_originalImage.TabIndex = 0;
            this.pictureBox_originalImage.TabStop = false;
            // 
            // Btn_Input
            // 
            this.Btn_Input.Location = new System.Drawing.Point(119, 333);
            this.Btn_Input.Name = "Btn_Input";
            this.Btn_Input.Size = new System.Drawing.Size(161, 23);
            this.Btn_Input.TabIndex = 1;
            this.Btn_Input.Text = "輸入原始影像";
            this.Btn_Input.UseVisualStyleBackColor = true;
            this.Btn_Input.Click += new System.EventHandler(this.Btn_Input_Click);
            // 
            // pictureBox_encryptImage
            // 
            this.pictureBox_encryptImage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.pictureBox_encryptImage.Location = new System.Drawing.Point(532, 3);
            this.pictureBox_encryptImage.Name = "pictureBox_encryptImage";
            this.pictureBox_encryptImage.Size = new System.Drawing.Size(413, 315);
            this.pictureBox_encryptImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_encryptImage.TabIndex = 2;
            this.pictureBox_encryptImage.TabStop = false;
            // 
            // Btn_encryption
            // 
            this.Btn_encryption.Location = new System.Drawing.Point(441, 134);
            this.Btn_encryption.Name = "Btn_encryption";
            this.Btn_encryption.Size = new System.Drawing.Size(75, 70);
            this.Btn_encryption.TabIndex = 3;
            this.Btn_encryption.Text = "影像加密→\r\n(ASGP)";
            this.Btn_encryption.UseVisualStyleBackColor = true;
            this.Btn_encryption.Click += new System.EventHandler(this.Btn_encryption_Click);
            // 
            // pictureBox_scrambleImage
            // 
            this.pictureBox_scrambleImage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.pictureBox_scrambleImage.Location = new System.Drawing.Point(541, 378);
            this.pictureBox_scrambleImage.Name = "pictureBox_scrambleImage";
            this.pictureBox_scrambleImage.Size = new System.Drawing.Size(413, 315);
            this.pictureBox_scrambleImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_scrambleImage.TabIndex = 4;
            this.pictureBox_scrambleImage.TabStop = false;
            // 
            // Btn_scramble
            // 
            this.Btn_scramble.Location = new System.Drawing.Point(518, 326);
            this.Btn_scramble.Name = "Btn_scramble";
            this.Btn_scramble.Size = new System.Drawing.Size(215, 46);
            this.Btn_scramble.TabIndex = 5;
            this.Btn_scramble.Text = "影像攪亂(位置交換法)↓\r\n(LRNG) ";
            this.Btn_scramble.UseVisualStyleBackColor = true;
            this.Btn_scramble.Click += new System.EventHandler(this.Btn_scramble_Click);
            // 
            // pictureBox_decryptImage
            // 
            this.pictureBox_decryptImage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.pictureBox_decryptImage.Location = new System.Drawing.Point(3, 378);
            this.pictureBox_decryptImage.Name = "pictureBox_decryptImage";
            this.pictureBox_decryptImage.Size = new System.Drawing.Size(413, 315);
            this.pictureBox_decryptImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_decryptImage.TabIndex = 6;
            this.pictureBox_decryptImage.TabStop = false;
            // 
            // Btm_recover
            // 
            this.Btm_recover.Location = new System.Drawing.Point(441, 529);
            this.Btm_recover.Name = "Btm_recover";
            this.Btm_recover.Size = new System.Drawing.Size(75, 49);
            this.Btm_recover.TabIndex = 7;
            this.Btm_recover.Text = "還原影像←";
            this.Btm_recover.UseVisualStyleBackColor = true;
            this.Btm_recover.Click += new System.EventHandler(this.Btm_recover_Click);
            // 
            // Btn_calculate
            // 
            this.Btn_calculate.Location = new System.Drawing.Point(227, 711);
            this.Btn_calculate.Name = "Btn_calculate";
            this.Btn_calculate.Size = new System.Drawing.Size(89, 23);
            this.Btn_calculate.TabIndex = 8;
            this.Btn_calculate.Text = "計算NPCR：";
            this.Btn_calculate.UseVisualStyleBackColor = true;
            this.Btn_calculate.Click += new System.EventHandler(this.Btn_calculate_Click);
            // 
            // Lab_NPCR
            // 
            this.Lab_NPCR.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.Lab_NPCR.Location = new System.Drawing.Point(370, 701);
            this.Lab_NPCR.Name = "Lab_NPCR";
            this.Lab_NPCR.Size = new System.Drawing.Size(194, 67);
            this.Lab_NPCR.TabIndex = 9;
            // 
            // Lab_red
            // 
            this.Lab_red.AutoSize = true;
            this.Lab_red.Location = new System.Drawing.Point(960, 618);
            this.Lab_red.Name = "Lab_red";
            this.Lab_red.Size = new System.Drawing.Size(97, 12);
            this.Lab_red.TabIndex = 10;
            this.Lab_red.Text = "R平面第一像素值";
            // 
            // Lab_green
            // 
            this.Lab_green.AutoSize = true;
            this.Lab_green.Location = new System.Drawing.Point(960, 647);
            this.Lab_green.Name = "Lab_green";
            this.Lab_green.Size = new System.Drawing.Size(97, 12);
            this.Lab_green.TabIndex = 11;
            this.Lab_green.Text = "G平面第一像素值";
            // 
            // Lab_blue
            // 
            this.Lab_blue.AutoSize = true;
            this.Lab_blue.Location = new System.Drawing.Point(960, 676);
            this.Lab_blue.Name = "Lab_blue";
            this.Lab_blue.Size = new System.Drawing.Size(97, 12);
            this.Lab_blue.TabIndex = 12;
            this.Lab_blue.Text = "B平面第一像素值";
            // 
            // Btn_ASGPcreate
            // 
            this.Btn_ASGPcreate.Location = new System.Drawing.Point(962, 257);
            this.Btn_ASGPcreate.Name = "Btn_ASGPcreate";
            this.Btn_ASGPcreate.Size = new System.Drawing.Size(121, 44);
            this.Btn_ASGPcreate.TabIndex = 13;
            this.Btn_ASGPcreate.Text = "產生ASGP種子檔";
            this.Btn_ASGPcreate.UseVisualStyleBackColor = true;
            this.Btn_ASGPcreate.Click += new System.EventHandler(this.Btn_ASGPcreate_Click);
            // 
            // Txt_primeNumber
            // 
            this.Txt_primeNumber.Location = new System.Drawing.Point(962, 218);
            this.Txt_primeNumber.Name = "Txt_primeNumber";
            this.Txt_primeNumber.Size = new System.Drawing.Size(100, 22);
            this.Txt_primeNumber.TabIndex = 14;
            // 
            // Btn_count
            // 
            this.Btn_count.Location = new System.Drawing.Point(190, 745);
            this.Btn_count.Name = "Btn_count";
            this.Btn_count.Size = new System.Drawing.Size(156, 23);
            this.Btn_count.TabIndex = 15;
            this.Btn_count.Text = "攪亂前後像素個數差異：";
            this.Btn_count.UseVisualStyleBackColor = true;
            this.Btn_count.Click += new System.EventHandler(this.Btn_count_Click);
            // 
            // Btn_replace
            // 
            this.Btn_replace.Location = new System.Drawing.Point(767, 326);
            this.Btn_replace.Name = "Btn_replace";
            this.Btn_replace.Size = new System.Drawing.Size(215, 46);
            this.Btn_replace.TabIndex = 16;
            this.Btn_replace.Text = "影像攪亂(像素取代法)↓\r\n(LRNG) ";
            this.Btn_replace.UseVisualStyleBackColor = true;
            this.Btn_replace.Click += new System.EventHandler(this.Btn_replace_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1165, 782);
            this.Controls.Add(this.Btn_replace);
            this.Controls.Add(this.Btn_count);
            this.Controls.Add(this.Txt_primeNumber);
            this.Controls.Add(this.Btn_ASGPcreate);
            this.Controls.Add(this.Lab_blue);
            this.Controls.Add(this.Lab_green);
            this.Controls.Add(this.Lab_red);
            this.Controls.Add(this.Lab_NPCR);
            this.Controls.Add(this.Btn_calculate);
            this.Controls.Add(this.Btm_recover);
            this.Controls.Add(this.pictureBox_decryptImage);
            this.Controls.Add(this.Btn_scramble);
            this.Controls.Add(this.pictureBox_scrambleImage);
            this.Controls.Add(this.Btn_encryption);
            this.Controls.Add(this.pictureBox_encryptImage);
            this.Controls.Add(this.Btn_Input);
            this.Controls.Add(this.pictureBox_originalImage);
            this.Name = "Form1";
            this.Text = "PRNG加密影像";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_originalImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_encryptImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_scrambleImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_decryptImage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox_originalImage;
        private System.Windows.Forms.Button Btn_Input;
        private System.Windows.Forms.PictureBox pictureBox_encryptImage;
        private System.Windows.Forms.Button Btn_encryption;
        private System.Windows.Forms.PictureBox pictureBox_scrambleImage;
        private System.Windows.Forms.Button Btn_scramble;
        private System.Windows.Forms.PictureBox pictureBox_decryptImage;
        private System.Windows.Forms.Button Btm_recover;
        private System.Windows.Forms.Button Btn_calculate;
        private System.Windows.Forms.Label Lab_NPCR;
        private System.Windows.Forms.Label Lab_red;
        private System.Windows.Forms.Label Lab_green;
        private System.Windows.Forms.Label Lab_blue;
        private System.Windows.Forms.Button Btn_ASGPcreate;
        private System.Windows.Forms.TextBox Txt_primeNumber;
        private System.Windows.Forms.Button Btn_count;
        private System.Windows.Forms.Button Btn_replace;
    }
}

