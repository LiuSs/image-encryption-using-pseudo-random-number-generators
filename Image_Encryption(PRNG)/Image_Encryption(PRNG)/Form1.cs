﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Image_Encryption_PRNG_
{
    public partial class Form1 : Form
    {
        //Global variable
        Random rand = new Random();
        Bitmap bitmap; //Image information
        SophieGermainPrimeGenerator sg = new SophieGermainPrimeGenerator();//Initialize the ASGP algo 
        LehmerRandomNumberGenerator lr = new LehmerRandomNumberGenerator();//Initialize the LRNG algo
        
        //Sub function block(Stage)
        //Get RGB plane pixel
        public int[,,] getRGBpixel(Bitmap oriImage)
        {
            // RGB plane are the same size , 0:R ,1:G,2:B
            int[,,] rgbPixel = new int[oriImage.Width, oriImage.Height, 3];
            for (int i = 0; i < bitmap.Width; i++)
            {
                for (int j = 0; j < oriImage.Height; j++)
                {
                    Color rgb = oriImage.GetPixel(i, j);
                    rgbPixel[i, j, 0] = rgb.R;
                    rgbPixel[i, j, 1] = rgb.G;
                    rgbPixel[i, j, 2] = rgb.B;
                }
            }

            //test  100 010  110
            Lab_red.Text = (4 ^ 2).ToString();
            Lab_green.Text = rgbPixel[0, 0, 1].ToString();
            Lab_blue.Text = rgbPixel[0, 0, 2].ToString();

            return rgbPixel;
        }

        //Generate the encrypting list : randList1 with SGP
        public int[] stage1Algo1(int[] sgpList,int height)
        {
            int[] randList1 = new int[height]; //size is "n"
            for(int i = 0; i < randList1.Length; i++)
            {
                randList1[i] = (sgpList[2 * height - i - 1] % sgpList[i]) % 256;
            }
            return randList1;
        }

        //Encrypt the image with stage1 algo2
        public Bitmap stage1Algo2(int[] randList1)
        {
            //get the image pixel
            int[,,] oriPixel = getRGBpixel(bitmap); // input is image information
            int[,,] encPixel = new int[bitmap.Width, bitmap.Height, 3];
            Bitmap encBitmap = new Bitmap(bitmap.Width, bitmap.Height);

            for (int i = 0; i < bitmap.Height; i++)
            {
                // Encryption column by column
                for (int j = 0; j < bitmap.Width; j++)
                {
                    encPixel[j, i, 0] = oriPixel[j, i, 0] ^ randList1[i];
                    encPixel[j, i, 1] = oriPixel[j, i, 1] ^ randList1[i];
                    encPixel[j, i, 2] = oriPixel[j, i, 2] ^ randList1[i];
                    encBitmap.SetPixel(j, i, Color.FromArgb(encPixel[j, i, 0], encPixel[j, i, 1], encPixel[j, i, 2]));
                }
            }
            return encBitmap;
        }

        //Generate the swap list : randList2 with LRN
        public int[] stage2Algo1(int[] lrnList,int height)
        {
            int[] randList2 = new int[height];
            int record = 0;
            for(int i = 0; i < height; i++)
            {
                if (lrnList[i] != 0)
                {
                    randList2[i] = lrnList[i];
                    record++;
                }
                if (lrnList[i] == 0)
                {                    
                    randList2[i] = lrnList[rand.Next(0, record - 1)];
                }                
            }
            return randList2;
        }

        //Scramble pixel with stage algo2
        public Bitmap stage2Algo2(int[] randList2)
        {
            pictureBox_scrambleImage.Image = pictureBox_encryptImage.Image;

            Bitmap encImage = new Bitmap(pictureBox_encryptImage.Image);
            Bitmap swapImage = new Bitmap(pictureBox_encryptImage.Image);
            //Bitmap swapImage = new Bitmap(encImage.Width, encImage.Height);
            int[,,] encPixel = getRGBpixel(encImage);
            //int[,,] swapPixel = new int[encImage.Width, encImage.Height, 3];


            for (int i = 0; i < encImage.Height; i++)
            {
                for (int j = 0; j < encImage.Width; j++)
                {
                    //Swap
                    //Console.WriteLine(randList2[i]);
                    //Console.WriteLine("尚未交換時："+encPixel[j,i,0]);
                    //Console.WriteLine("要交換的像素：" + encPixel[j, randList2[i], 0]);

                    int tmpR = encPixel[j, i, 0];//R plane
                    encPixel[j, i, 0] = encPixel[j, randList2[i], 0];
                    encPixel[j, randList2[i], 0] = tmpR;

                    //Console.WriteLine("交換後：" + encPixel[j, i, 0]);
                    //Console.WriteLine("被換後：" + encPixel[j, randList2[i], 0]);

                    int tmpG = encPixel[j, i, 1];//G plane
                    encPixel[j, i, 1] = encPixel[j, randList2[i], 1];
                    encPixel[j, randList2[i], 1] = tmpG;

                    int tmpB = encPixel[j, i, 2];//B plane
                    encPixel[j, i, 2] = encPixel[j, randList2[i], 2];
                    encPixel[j, randList2[i], 2] = tmpB;
                }
            }

            for (int i = 0; i < encImage.Height; i++)
            {
                for (int j = 0; j < encImage.Width; j++)
                {
                    swapImage.SetPixel(j, i, Color.FromArgb(encPixel[j, i, 0], encPixel[j, i, 1], encPixel[j, i, 2]));
                    //encImage.SetPixel(j, i, Color.FromArgb(encPixel[j, i, 0], encPixel[j, i, 1], encPixel[j, i, 2]));
                    //Console.WriteLine("加密後的RGB：(" + encPixel[j, i, 0] + "," + encPixel[j, i, 1] + "," + encPixel[j, i, 2] + ")");
                    //Console.WriteLine("被設定後的RGB：(" + swapImage.GetPixel(j,i).R+ "," + swapImage.GetPixel(j, i).G + ","  + swapImage.GetPixel(j, i).B+ ")");

                }
            }

            //return encImage;
            return swapImage;
        }

        //Replace pixel with stage algo2
        public Bitmap stage2Algo2Replace(int[] randList2)
        {
            Bitmap encImage = new Bitmap(pictureBox_encryptImage.Image);
            Bitmap swapImage = new Bitmap(pictureBox_encryptImage.Image);
            //Bitmap swapImage = new Bitmap(encImage.Width, encImage.Height);
            int[,,] encPixel = getRGBpixel(encImage);
            //int[,,] swapPixel = new int[encImage.Width, encImage.Height, 3];


            for (int i = 0; i < encImage.Height; i++)
            {
                for (int j = 0; j < encImage.Width; j++)
                {
                    //Replace                                   
                    encPixel[j, i, 0] = randList2[encPixel[j, i, 0]];
                    encPixel[j, i, 1] = randList2[encPixel[j, i, 1]];
                    encPixel[j, i, 2] = randList2[encPixel[j, i, 2]];
                }
            }

            for (int i = 0; i < encImage.Height; i++)
            {
                for (int j = 0; j < encImage.Width; j++)
                {
                    swapImage.SetPixel(j, i, Color.FromArgb(encPixel[j, i, 0], encPixel[j, i, 1], encPixel[j, i, 2]));
                    //encImage.SetPixel(j, i, Color.FromArgb(encPixel[j, i, 0], encPixel[j, i, 1], encPixel[j, i, 2]));
                    //Console.WriteLine("加密後的RGB：(" + encPixel[j, i, 0] + "," + encPixel[j, i, 1] + "," + encPixel[j, i, 2] + ")");
                    //Console.WriteLine("被設定後的RGB：(" + swapImage.GetPixel(j,i).R+ "," + swapImage.GetPixel(j, i).G + ","  + swapImage.GetPixel(j, i).B+ ")");

                }
            }

            //return encImage;
            return swapImage;
        }

        //De-Swap pixel(replace)
        public Bitmap stage2Algo2deReplace(int[] randList2)
        {
            Bitmap encImage = new Bitmap(pictureBox_encryptImage.Image);
            Bitmap swapImage = new Bitmap(pictureBox_encryptImage.Image);
            //Bitmap swapImage = new Bitmap(encImage.Width, encImage.Height);
            int[,,] encPixel = getRGBpixel(encImage);
            //int[,,] swapPixel = new int[encImage.Width, encImage.Height, 3];


            for (int i = 0; i < encImage.Height; i++)
            {
                for (int j = 0; j < encImage.Width; j++)
                {
                    //Replace                                   
                    encPixel[j, i, 0] = randList2[encPixel[j, i, 0]];
                    encPixel[j, i, 1] = randList2[encPixel[j, i, 1]];
                    encPixel[j, i, 2] = randList2[encPixel[j, i, 2]];
                }
            }

            for (int i = 0; i < encImage.Height; i++)
            {
                for (int j = 0; j < encImage.Width; j++)
                {
                    swapImage.SetPixel(j, i, Color.FromArgb(encPixel[j, i, 0], encPixel[j, i, 1], encPixel[j, i, 2]));
                    //encImage.SetPixel(j, i, Color.FromArgb(encPixel[j, i, 0], encPixel[j, i, 1], encPixel[j, i, 2]));
                    //Console.WriteLine("加密後的RGB：(" + encPixel[j, i, 0] + "," + encPixel[j, i, 1] + "," + encPixel[j, i, 2] + ")");
                    //Console.WriteLine("被設定後的RGB：(" + swapImage.GetPixel(j,i).R+ "," + swapImage.GetPixel(j, i).G + ","  + swapImage.GetPixel(j, i).B+ ")");

                }
            }

            //return encImage;
            return swapImage;
        }

        //Main function 
        public Form1()
        {
            InitializeComponent();
        }             

        private void Btn_Input_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog(); //create file object
                openFileDialog.Filter = "All File|*.*"; // set the file format
                openFileDialog.ShowDialog(); // give the tip to the user

                bitmap = new Bitmap(openFileDialog.FileName); // let the variable have the file imformation
                pictureBox_originalImage.Image = bitmap;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }            
        }

        private void Btn_encryption_Click(object sender, EventArgs e)
        {
            try
            {
                // generate Sophie Germain Prime list , size (0,2n) 
                int[] ASGP = sg.sgpGenerator(bitmap.Height);

                // A. Stage1 Algo : 
                //Generate the randList1
                int[] randList1 = stage1Algo1(ASGP,bitmap.Height);  // size is "n"

                // A. Stage1 Algo :
                // Encrypt image with randList1
                Bitmap encBitmap = stage1Algo2(randList1);                
                pictureBox_encryptImage.Image = encBitmap; //show the encrypted image int the picture box
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Btn_scramble_Click(object sender, EventArgs e)
        {
            //B. Stage2 Algo :
            //Generate the key of LRNG
            int[] lrngKey = lr.lrngKeyCreate(bitmap.Height);
            int[] LRNG = lr.lrnGenerator(lrngKey[0], lrngKey[1], lrngKey[2]); //Get randList2

            //B. Stage2 Algo :
            //Scramble pixel with randList2
            int[] randList2 = stage2Algo1(LRNG,bitmap.Height);
            Bitmap swapBitmap = stage2Algo2(randList2);

            pictureBox_scrambleImage.Image = swapBitmap;
        }

        private void Btn_ASGPcreate_Click(object sender, EventArgs e)
        {                   
            sg.sgpSeedGenerator(Convert.ToInt32(Txt_primeNumber.Text));
        }

        private void Btn_calculate_Click(object sender, EventArgs e)
        {
            //Lab_NPCR.Text = lr.getNumberOfCoprime(Convert.ToInt32(Txt_primeNumber.Text)).ToString();
            Lab_NPCR.Text ="a:" +lr.lrngKeyCreate(Convert.ToInt32(Txt_primeNumber.Text))[0].ToString()+",X0"+ lr.lrngKeyCreate(Convert.ToInt32(Txt_primeNumber.Text))[1].ToString()+",m:"+ lr.lrngKeyCreate(Convert.ToInt32(Txt_primeNumber.Text))[2].ToString();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Btn_count_Click(object sender, EventArgs e)
        {
            int encCount = 0;
            int swapCount = 0;           

            int encH = pictureBox_encryptImage.Image.Height;
            int encW = pictureBox_encryptImage.Image.Width;

            int swaH = pictureBox_scrambleImage.Image.Height;
            int swaW = pictureBox_scrambleImage.Image.Width;

            int encR = 0;
            int encG = 0;
            int encB = 0;
            int swaR = 0;
            int swaG = 0;
            int swaB = 0;

            Bitmap encIma = new Bitmap(pictureBox_encryptImage.Image);
            Bitmap swaIma = new Bitmap(pictureBox_scrambleImage.Image);

            MessageBox.Show("加密大小：" + encIma.Width + "," + encIma.Height + "，擾亂大小：" + swaIma.Width + "," + swaIma.Height);

            for (int i = 0; i < encW; i++)
            {
                for(int j = 0; j < encH; j++)
                {
                    encR = encR + encIma.GetPixel(i, j).R;
                    encG = encG + encIma.GetPixel(i, j).G;
                    encB = encB + encIma.GetPixel(i, j).B;
                    encCount = encCount + encIma.GetPixel(i, j).R+ encIma.GetPixel(i, j).G+ encIma.GetPixel(i, j).B;
                }
            }

            for (int i = 0; i < swaW; i++)
            {
                for (int j = 0; j < swaH; j++)
                {
                    swaR = swaR + swaIma.GetPixel(i, j).R;
                    swaG = swaG + swaIma.GetPixel(i, j).G;
                    swaB = swaB + swaIma.GetPixel(i, j).B;
                    swapCount = swapCount + swaIma.GetPixel(i, j).R+ swaIma.GetPixel(i, j).G+ swaIma.GetPixel(i, j).B;
                }
            }
            MessageBox.Show("EncR:" + encR + ", SwapR:" + swaR);
            MessageBox.Show("EncG:" + encG + ", SwapG:" + swaG);
            MessageBox.Show("EncB:" + encB + ", SwapB:" + swaB);
            MessageBox.Show("加密的綜合像素：" + encCount + "，交換後綜合像素：" + swapCount);
        }

        private void Btn_replace_Click(object sender, EventArgs e)
        {
            //B. Stage2 Algo :
            //Generate the key of LRNG
            int[] lrngKey = lr.lrngKeyCreate(256);
            int[] LRNG = lr.lrnGenerator(lrngKey[0], lrngKey[1], 256); //Get randList2

            //B. Stage2 Algo :
            //Scramble pixel with randList2
            int[] randList2 = stage2Algo1(LRNG, 256);
            Bitmap replaceBitmap = stage2Algo2Replace(randList2);

            pictureBox_scrambleImage.Image = replaceBitmap;
        }

        private void Btm_recover_Click(object sender, EventArgs e)
        {
            //Step1 : LRNG
            int[] lrngKey = lr.lrngKeyCreate(255);
            int[] LRNG = lr.lrnGenerator(lrngKey[0], lrngKey[1], 256); //Get randList2


        }
    }
}
