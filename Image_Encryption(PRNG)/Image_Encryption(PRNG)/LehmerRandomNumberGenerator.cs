﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Numerics;

namespace Image_Encryption_PRNG_
{
    class LehmerRandomNumberGenerator
    {
        Random rand = new Random();
        SophieGermainPrimeGenerator sg = new SophieGermainPrimeGenerator();

        public int[] lrnGenerator(int X0,int g,int n)
        {
            int[] LRNG = new int[n];
            int count = 1; //0 is X0
            LRNG[0] = X0;
            int Xnext = (g * LRNG[0]) % n;

            try
            {                
                while (Xnext != LRNG[0])
                {
                    LRNG[count] = Xnext;
                    count++;
                    Xnext = (g * LRNG[count-1]) % n;         
                }
                return LRNG;
            }
           
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                return LRNG;
            }                       
           
        }

        public int[] lrngKeyCreate(int n)
        {
            //Lehmer Random Number : X(m+1) = g*Xm mod n

            int[] lrngKey = new int[3]; // 0:X0 , 1:g , 2:n            

            //generate g : multiplicative order 
            //find Primitive root
            int coprimeNum = getNumberOfCoprime(n);
            int g = findPrimitiveRoot(coprimeNum, n);

            //find X0 , X0 is co-prime with n
            int X0=2; //assume
            while (isCoprime(X0, n) == false)
            {
                X0++;
            }

            return new int[]{X0,g,n};
        }

        //find primitive root
        public int findPrimitiveRoot(int d,long m)
        {
            int g = 1;
            int[] rootPeriod = new int[m];
           // int flag = 0;
            long firstValue = 0;

            for (int i = 2; i < m; i++) // 1 is not primitive root
            {
                if (isCoprime(BigInteger.Pow(i,d), m) == true)
                {
                    firstValue = fastModulo(i,1,m);
                    for (int j = 2; j < (d+1); j++)
                    {
                        if (fastModulo(i, j, m) != firstValue)
                        {
                            rootPeriod[i]++;                           
                        }                              
                                
                        //if ((fastModulo(i, d, m) == firstValue) && j != d)
                        //{
                        //    rootPeriod[i] = j-1; //record the period
                        //    flag = 1;
                        //}                        
                        //else if ((fastModulo(i, d, m) == 1) && j == d && flag==0)
                        //{
                        //    g = i;
                        //    return g;
                        //}
                        //else
                        //{                           
                        //    int maxIndex = findMax(rootPeriod);
                        //    g = maxIndex;
                        //}
                    }
                }                
              //  flag = 0;
            }
            g = findMax(rootPeriod);
            return g;
        }

        // a*b mod p = (a mod p) * (b mod p)
        //fast get modulo
        public long fastModulo(long a,int power,long modulo)
        {
            BigInteger bitNumber = BigInteger.Pow(a, power);
            return (int)(BigInteger.Remainder(bitNumber,modulo));
        }

        public int findMax(int[] number)
        {
            int max = number[0];
            int maxIndex = 0;
            for(int i = 0; i < number.Length; i++)
            {
                if (number[i] > max)
                {
                    max = number[i];
                    maxIndex = i;
                }
            }
            Console.WriteLine("最大循環量：" + max + "，此時的原根(或循環值)：" + maxIndex);
            return maxIndex;
        }

        //check whether co-prime
        public bool isCoprime(BigInteger n1,BigInteger n2)
        {
            if (gcd(n1, n2) == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public BigInteger gcd(BigInteger a, BigInteger b)
        {
            //if (b > a)
            //{
            //    int tmp;
            //    tmp = a;
            //    a = b;
            //    b = tmp;
            //}

            while (b != 0)
            {

                BigInteger tmp;
                tmp = b;
                b = a % b;
                a = tmp;
            }
            return a;
        }

        //get the co-prime number d where less m with m
        public int getNumberOfCoprime(int modulo)
        {
            int count = 0;            
            for(int i = 1; i < modulo; i++)
            {
                if (isCoprime(i, modulo) == true)
                {
                    //Console.WriteLine(i);
                    count++;
                }
            }
            return count;
        }
    }
}
