﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;

namespace Image_Encryption_PRNG_
{
    class SophieGermainPrimeGenerator
    {        
        // Main SGP create , select 4096 SGP prime form the txt
        public int[] sgpGenerator(int h)
        {
            //read the "ASGP.txt" to get SGP number
            int[] SGPseed = new int[2 * h];            
            int countSGP = 0;
            StreamReader sr = new StreamReader("ASGP.txt");
            int[] intSGP = new int[4096];

            while (true)
            {
                string tmp;
                if ((tmp = sr.ReadLine()) == null)
                {
                    break;
                }
                intSGP[countSGP] = Convert.ToInt32(tmp);
                countSGP++;

                if (countSGP == intSGP.Length)
                {
                    break;
                }
            }

            Random rand = new Random(); //initialize the ramdom object
            for(int i = 0; i < SGPseed.Length; i++)
            {                
                int n = rand.Next(0, intSGP.Length-1);
                SGPseed[i] = intSGP[n];
            }
  
            return SGPseed;
        }

        // Create the SGP "seed"
        public void sgpSeedGenerator(int number)
        {
            try
            {
                //according to the number to create SGP
                int count = 0;
                int prime = 2;
                string[] ASGPseed = new string[number];

                while (count < number)
                {
                    if (isPrime(prime) == true && isPrime(2 * prime + 1))
                    {
                        ASGPseed[count] = prime.ToString();
                        count++;
                    }
                    prime++;
                }

                // Open the text file using a stream reader.
                // the file is in the "bin" folder
                System.IO.File.WriteAllLines("ASGP.txt", ASGPseed);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        //Judge prime 
        public bool isPrime(int p)
        {
            if (p == 1)
            {
                return false;
            }

            for(int i = 2; i * i <= p; i++)
            {
                if (p % i == 0)
                {
                    return false;
                }
            }
            return true;
        }
    }
}
